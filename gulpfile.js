"use strict";

const gulp = require('gulp-help')(require('gulp'));
const del = require('del');
const fs = require('fs');
const inlinesource = require('gulp-inline-source');
const gutil = require('gulp-util');
const runSequence = require('run-sequence');
const sitemap = require('gulp-sitemap');
const child_process = require('child_process');
const execSync = child_process.execSync;
const exec = child_process.exec;
const zopfli = require("gulp-zopfli");
const nunjucksRender = require('gulp-nunjucks-render');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const rev = require('gulp-rev');
const cache = require('gulp-cached');
const argv = require('yargs').argv;
const webserver = require('gulp-webserver');

const conf = require('./config');
const paths = conf.paths;

var child_processes = [];

gulp.task('html', ['assets', 'rev'], function() {
    const rev_manifest = JSON.parse(fs.readFileSync(paths.asset_rev.manifest(), 'utf8'));
    
    var manageNunjucksEnvironment = function(environment) {
      environment.addFilter(conf.nunjucks_filter_revedStaticPath, function(str) {
        if (str.startsWith('/')) str = str.substring(1);
        var revedPath = rev_manifest[str];
        if (!revedPath)
          throw new gutil.PluginError({
            plugin: 'html',
            message: 'No reved version for ' + str + ' found.'
          });
        return paths.asset_rev.uri() + revedPath;
      });
    }

    return gulp.src(paths.src.html())
      .pipe(nunjucksRender({
          path: [conf.paths.nunjucks_base()],
          manageEnv: manageNunjucksEnvironment
      }))
      .pipe(inlinesource({
          rootpath: paths.inlinesource_base(),
          compress: false
      }))
      .pipe(gulp.dest(paths.dest.html()))
})

gulp.task('assets', ['assets:images', 'assets:svg', 'assets:styles', 'assets:js']);

gulp.task('assets:images', function() {
  return gulp.src(paths.src.img())
    .pipe(cache('assets:images'))
    .pipe(imagemin())
    .pipe(gulp.dest(paths.dest.img()))
});

gulp.task('assets:svg', function() {
  return gulp.src(paths.src.svg())
    .pipe(cache('assets:svg'))
    .pipe(imagemin())
    .pipe(gulp.dest(paths.dest.svg()))
});


gulp.task('assets:styles', function() {
  return gulp.src(paths.src.scss())
    .pipe(cache('assets:styles'))
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 3 version', 'ie10'))
    //.pipe(gulp.dest('public/static/css/')) // don't output non-minified ones
    .pipe(cleanCSS({compatibility: '*', level: 2}))
    .pipe(rename({extname: '.min.css'}))
    .pipe(gulp.dest(paths.dest.css()));
});

gulp.task('assets:js', function() {
  return gulp.src(paths.src.js())
    .pipe(cache('assets:js'))
    //.pipe(gulp.dest('public/static/js/')) // don't output non-minified ones
    .pipe(uglify())
    .pipe(rename({extname: '.min.js'}))
    .pipe(gulp.dest(paths.dest.js()));
});

gulp.task('sitemap', function () {
  return gulp.src(paths.src.sitemap(), {
      read: false
    })
    .pipe(sitemap({
      siteUrl: conf.sitemap.getSiteUrl(),
      lastmod: function(file) {
        var cmd = 'git log -1 --format=' + conf.git_log_iso_date_formatstring + ' "' + paths.src.common() + file.relative + '"';
        return execSync(cmd).toString().trim();
      },
      mappings: conf.sitemap.mappings
    }))
    .pipe(gulp.dest(paths.dest.sitemap()));
});

gulp.task('copy:xml', function() {
  return gulp.src(paths.src.xml())
    .pipe(cache('copy:xml'))
    .pipe(gulp.dest(paths.dest.xml()))
});

gulp.task('rev', ['assets'], function() {
  return gulp.src(paths.src.rev(), {base: paths.src.rev_base()})
    .pipe(cache('rev'))
    .pipe(rev())
    .pipe(gulp.dest(paths.dest.rev()))
    /* According to gulp-rev docs this should be:

    .pipe(rev.manifest(paths.asset_rev.manifest_name(), {
      base: paths.dest.rev_manifest(),
      merge: true}))
    .pipe(gulp.dest(paths.dest.rev_manifest()));

    but somehow it does not work.
    */
    .pipe(rev.manifest(paths.asset_rev.manifest(), {
      merge: true}))
    .pipe(gulp.dest('./'));
});

gulp.task('clean:nonreved', function() {
  return del([paths.src.del_nonreved()]);
});

gulp.task('compress', function() {
  return gulp.src(paths.src.compress())
    .pipe(cache('compress'))
    .pipe(zopfli())
    .pipe(gulp.dest(paths.dest.compress()));
});

gulp.task('clean', function() {
  cache.caches = {};
  return del(paths.src.clean());
});

gulp.task('dirty:styles', function(callback) {
  delete cache.caches['assets:styles'];
  runSequence('html', callback); // assets:styles is a dependency of html so it will run here
});

gulp.task('dirty:js', function(callback) {
  delete cache.caches['assets:js'];
  runSequence('html', callback); // assets:js is a dependency of html so it will run here
});

gulp.task('debug:watch', 'watch for changes and update accordingly', function() {
  gulp.watch(paths.watch.html(), ['html']);
  gulp.watch(paths.src.img(), ['assets:images', 'html']);
  gulp.watch(paths.src.svg(), ['assets:svg', 'html']);
  gulp.watch(paths.src.scss(), ['dirty:styles']);
  gulp.watch(paths.src.js(), ['dirty:js']);
});

gulp.task('debug:serve', ['debug:serve:backend', 'debug:serve:static']);

gulp.task('debug:serve:backend', function() {
  conf.serve.backends_exec.forEach(function(cmd) {
    child_processes.push(exec(cmd));
  });
});

gulp.task('debug:serve:static', 'starts a local webserver (--port specifies bound port)', function() {
  const port = argv.port || 8000;
  
  gulp.src(paths.dest.common())
    .pipe(webserver({
      livereload: true,
      port: port,
      fallback: 'index.html',
      proxies: conf.proxies
    }));
});

gulp.task('debug', function(callback) {
  runSequence('clean',
              'html',
              'clean:nonreved',
              'copy:xml',
              'debug:watch',
              'debug:serve',
              callback);
});

gulp.task('build', function(callback) {
  runSequence('clean',
              'html',
              'clean:nonreved',
              'sitemap',
              'copy:xml',
              'compress',
              callback);
});

gulp.task('default', ['build']);

process.on('exit', function() {
  const len = child_processes.length;
  if (len > 0) {
    console.log('Killing', child_processes.length, 'child process(es)...');
    child_processes.forEach(function(child) {
      child.kill();
    });
  }
  console.log('Exiting...');
});

var cleanExit = function() { process.exit() };
process.on('SIGINT', cleanExit); // catch ctrl-c
process.on('SIGTERM', cleanExit); // catch kill
