# gulp-static-site-template

## When do I need this?
* You want to build a very fast static website.
* You don't want to setup the complete build stack from scratch.
* You like static websites but want comfort while creating them (=> Templates, SCSS).
* You want a very secure and fast webserver setup (basically just nginx is needed to serve this).
* You're okay with having many javascript dependencies. I always searched for the most reasonable one.
* You want to have [Gulp](https://gulpjs.com/) as base of your static site generation stack.
* You want develop fast and efficient. The integrated webserver with live reloading is for you.

## Why
As a developer I don't really enjoy building websites. But if I have to, I want to build them properly, which means including templates, minification, high cache timeouts, good <a href="https://developers.google.com/speed/pagespeed/insights/">PageSpeed Insights ranking</a> and so on. Additionally I always try to build static websites. If you need no dynamic script-interpreting server-side parts, things speed up a lot and the number of CVEs you should be concerned about reduces a lot too. When I started my last static website project, I was looking for a solution providing me with all this - hopefully doing most of it automatically. As I didn't find anything that fit my imagination, I started building it myself.

## What it does / Features
* **It's very simple**. You can start writing HTML right away and you will get quite much optimized results though.
* **Templates**. No one want's to copy his basic website design around. That's why you can use [Nunjucks](https://mozilla.github.io/nunjucks/) as a powerful template engine.
* **SCSS**. Write [SCSS](http://sass-lang.com/) instead of CSS.
* **Minification**. Javascript, CSS and images become minified, uglified, optimized.
* **Inlining**. Inline small resources without any hassle. Just use this syntax: `<link inline href="...`.
* **Revisioning**. All your static resources are renamed to include a hash in their filename. Set the files to [never expire](https://developer.yahoo.com/performance/rules.html#expires). Link your resources using this syntax: `<link href="{{ '/static/css/<yourFileName>.min.css' | rev }}" rel="stylesheet">`.
* **Auto-prefixing**. The cross-browser-compatibility of your CSS is optimized using [Autoprefixer](https://github.com/postcss/autoprefixer)
* **Pre-compressing**. Google's [Zopfli](https://github.com/google/zopfli) algorithm is used to compress all generated files.
* **Sitemap generation**. A sitemap containing all your html files is generated automatically.
* **Integrated Webserver**. If you run `yarn debug` you get a webserver listening at `localhost:8000`, which servers your site.
* **Live Reload**. When using the integrated webserver your site will automatically refresh as you change one or more of your sources.

## Getting Started
1. Clone this repository
2. Ensure you have yarn, nodejs and python2 installed in your path. Or simply stick to CI to generate your website.
3. Change the values in `package.json` to reflect your project (name, license, ...)
4. Change the `sitemap_siteUrl` in `config.js`. Otherwise your generated sitemaps will be invalid.
5. Replace the contents of the `src` folder with yours.
6. Run `yarn install` and `yarn debug`.
7. Browse to `localhost:8000` (the port is shown in your terminal) to visit your freshly generated website.
8. You could rename the CI job to `pages` to have your generated website published to GitLab Pages.

### Example
See the `src` dirtectory in this repository for a very lightweight example.

## Folder Structure
* `src`: This folder holds your website's contents including all assets
  * **HTML** files live directly in this directory. You can create custom subdirectories to create a more complicated structure too.
  * `_tpl`: Your nunjucks base templates and reused parts like menus are placed here.
  * `img`: Copy your pixel-based images here.
  * `js`: JavaScript files.
  * `scss`: All *SCSS* files placed here are passed over to sass.
  * `svg`: SVG files (vector graphics).
* `public`: This folder is created by the build process. Be warned, it get's deleted (with all it's contents) during the build process too.
* `build`: This folder is created by the build process and deleted afterwards. Don't place any of your files here. It holds unminified versions of some assets during the build process.

Of corse you can modify and extend this structure as you like. This is what I thought makes sense for most projects.

## Hosting
Running `yarn build` creates a directory named `public`. You can copy it's contents to your webserver. It makes a lot of sense to serve `/rev` with the maximum possible cache expiration time. I preffer using NGinx as a webserver. To use the pre-compressed files that we generate you should turn [gzip_static](http://nginx.org/en/docs/http/ngx_http_gzip_static_module.html) `on`.

## Speed
If you configure your webserver properly and use inlining at reasonable places, you will get a blazingly fast website. Check your results using [Google's PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/) and use the Chrome Developer Tools. Using this template I could easily achieve **100 out of 100 points at PageSpeed Insights**.

## What it lacks
* More documentation than this file for now. This is meant to be a template for your own website project. Possibly you will need to change some of the tasks.
* Maybe a more complicated example, that is affected by more of the applied optimizations.
* You could extend this to utilize [UnCSS](https://www.npmjs.com/package/gulp-uncss). You may want to be careful using it, as it can destroy your layout if you use JavaScript to change parts of it.

Feel free to change or extend the code in this repository and send me a merge request.

2017 [Georg Jung](gulp-static-site-template@gjung.com)