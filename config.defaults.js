var config = {};

config.location = 'https://example.com';

config.proxies = [];

const sitemap = {
  mappings: [
    {
      pages: [ 'index.html' ],
      priority: 1.0
    }
  ],
  getSiteUrl: () => config.location
};
config.sitemap = sitemap;

const serve = {
    backends_exec: []
};
config.serve = serve;

config.nunjucks_filter_revedStaticPath = 'rev';
config.git_log_iso_date_formatstring = '%ci';

config.paths = {};
var paths = config.paths;

paths.build = {};
paths.build.common = () => 'build/';

paths.asset_rev = {
    manifest_name: () => 'rev-manifest.json',
    foldername: () => 'rev'
};

paths.dest = {};
paths.dest.common = () => 'public/';
paths.dest.html = () => paths.dest.common();
paths.dest.static_files = () => paths.build.common() + 'static/';
paths.dest.img = () => paths.dest.static_files() + 'img/';
paths.dest.svg = () => paths.dest.static_files() + 'svg/';
paths.dest.css = () => paths.dest.static_files() + 'css/';
paths.dest.js = () => paths.dest.static_files() + 'js/';
paths.dest.sitemap = () => paths.dest.common();
paths.dest.rev = () => paths.dest.common() + paths.asset_rev.foldername() + '/';
paths.dest.rev_manifest = () => paths.dest.common();
paths.dest.compress = () => paths.dest.common();
paths.dest.xml = () => paths.dest.common();

paths.src = {};
paths.src.common = () => 'src/';
paths.src.html = () => paths.src.common() + '*.html';
paths.src.img = () => paths.src.common() + 'img/*';
paths.src.svg = () => paths.src.common() + 'svg/*';
paths.src.scss = () => paths.src.common() + 'scss/*.scss';
paths.src.js = () => paths.src.common() + 'js/*.js';
paths.src.sitemap = () => [paths.dest.html() + '*.html'];
paths.src.rev = () => paths.build.common() + '**';
paths.src.rev_base = () => paths.build.common();
paths.src.del_nonreved = () => paths.src.rev();
paths.src.compress = () => [paths.dest.common() + '**', '!*.png', '!*.jpg', '!*.gif'];
paths.src.clean = () => paths.dest.common() + '**';
paths.src.xml = () => paths.src.common() + '*.xml';

paths.watch = {};
paths.watch.html = () => paths.src.common() + '**/*.html';
paths.watch.serve = () => paths.dest.common() + '**/*';

paths.asset_rev.manifest = () => paths.dest.rev_manifest() + paths.asset_rev.manifest_name();
paths.asset_rev.uri = () => '/' + paths.asset_rev.foldername() + '/';

paths.inlinesource_base = () => paths.dest.common();
paths.nunjucks_base = () => paths.src.common();

module.exports = config;
